import 'package:flutter/material.dart';
import 'package:helloworld/view/landing.dart';
import 'package:helloworld/view/login.dart';
import 'package:helloworld/view/content/listDescription.dart';
import 'package:helloworld/view/content/infoList.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //title: 'App Rest',
      theme: ThemeData(),
      initialRoute: '/',
      routes: {
        '/': (context) => LandingPage(),
        '/login': (context) => Login(),
        '/listDescription': (context) => ListDescription(),
        //_InfoList.routeName: (context) => _InfoList()
        '/infolist': (context) => InfoList(),
      },
    );
  }
}

