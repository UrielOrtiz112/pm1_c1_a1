import 'package:flutter/material.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final imageLogo = Material(
      elevation: 5.0,
      child: Image.network(
          'https://googleflutter.com/sample_image.jpg'
      ),
    );
    final text = Material(
      child: Text(
        "Bienvenido",
        textAlign: TextAlign.center,
      ),
    );
    final goButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color.fromRGBO(42, 200, 194, 1),
      child: MaterialButton(
        //minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          //_showMyDialog(_username.text, _passwordField.text);
          Navigator.pushNamed(context, '/login');
        },
        child: Text(
          "Iniciar",
          textAlign: TextAlign.center,
        ),
      ),
    );
    return Scaffold(
      body: Center(
        child: Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 50.0, 20.0, 45.0),
                  //padding: EdgeInsets.all(40),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      imageLogo,
                      SizedBox(
                        height: 10.0,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      text,
                      SizedBox(
                        height: 15.0,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(40),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      goButton,
                      SizedBox(
                        height: 15.0,
                      ),
                    ],
                  ),
                )
              ],
            )
        ),
      ),
    );
  }
}