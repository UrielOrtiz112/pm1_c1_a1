import 'package:flutter/material.dart';
import 'package:helloworld/model/listDescription.dart';

class InfoList extends StatelessWidget {
  static const routeName = '/listDescription';
  final Todo todo;
  InfoList({Key key, @required this.todo}) : super(key: key);
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(240, 94, 100, 1),
        title: Text('Información'),
      ),
      body: Center(
        child: Text(
            todo.username
        ),
      ),
    );
  }
}
