import 'package:flutter/material.dart';
import 'package:helloworld/view/content/infoList.dart';
import 'package:helloworld/model/listDescription.dart';

class ListDescription extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final todos = List<Todo>.generate(
      20,
          (i) => Todo(
        "User $i".toString(),
      ),
    );
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(240, 94, 100, 1),
        title: Text('Lista'),
      ),
      body: ListView.builder(
        itemCount: todos.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(todos[index].username),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => InfoList(todo: todos[index]),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
