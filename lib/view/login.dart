import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  final _username = TextEditingController();
  final _passwordField = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Future<void> _showMyDialog(String username, String password) async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('Hola!'),
                  Text('Bienvenido $username'),
                  Text('Contraseña: $password')
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('OK!'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    var correo = TextField(
      controller: _username,
      obscureText: false,
      decoration: InputDecoration(
          fillColor: Colors.white, //aqui
          filled: true, //y aqui
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Usuario",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final passwordField = TextField(
      controller: _passwordField,
      obscureText: true,
      decoration: InputDecoration(
          fillColor: Colors.white, //aqui
          filled: true, //y aqui
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Contraseña",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color.fromRGBO(42, 200, 194, 1),
      child: MaterialButton(
        //minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          //_showMyDialog(_username.text, _passwordField.text);
          Navigator.pushNamed(context, '/listDescription');
        },
        child: Text(
          "Iniciar sesión",
          textAlign: TextAlign.center,
        ),
      ),
    );
    final backButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color.fromRGBO(42, 200, 194, 1),
      child: MaterialButton(
        //minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pushNamed(context, '/');
        },
        child: Text(
          "Atras",
          textAlign: TextAlign.center,
        ),
      ),
    );
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(240, 94, 100, 1),
        title: Text("Login"),
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(30, 150, 30, 100),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 45.0),
                    correo,
                    SizedBox(height: 25.0),
                    passwordField,
                    SizedBox(
                      height: 35.0,
                    ),
                    loginButon,
                    SizedBox(
                      height: 15.0,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: 200),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    backButon,
                    SizedBox(
                      height: 15.0,
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}
